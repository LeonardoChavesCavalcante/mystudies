package app2;
public class Operator{

	private String binaries = "+, -, *, /, %, = ...";

	private String unaries = "++, --";

	 private String ternaries = "?";

	private String  relationals = "< ,> <=, >=, instanceOf ";


	public String getBinaries(){

		return this.binaries;
	}

	public String getUnaries(){

		return this.unaries;
	}

	public String  getTernaries(){

		return this.ternaries;
	}

	public void  printRules(){

		System.out.println(" 1° Unary");
		System.out.println(" 2° Binary");
	}


	public void  testResult1(){

		byte x = 10;
		long  y = 19999999998L;
		int t = 1921222;
		short s = (short)1921222;
                double z = 1 + 5.3f;
		int k = (x=8)+(t=1);
               /* y = (y != 18) ? 51 : 100;*/
 		System.out.println("Resultado 1");
		System.out.println(y);
		System.out.println(Integer.toBinaryString(t));
		System.out.println(k);
	}

	public void testResult2(){
		boolean check = !true;
		check = (1 == 2) & check;

                System.out.println("Resultado 2");
		System.out.println(check);
	}


}
