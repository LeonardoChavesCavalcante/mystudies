package app2;

import app2.Operator;// isso aqui não é necessário devido a classe Operator estar no mesmo package;

public class Application2{

	public static void main(String ...args){

		System.out.println("Hello guy " +args[0].toString());
		if (args[0].equals("operator") ){
	       		Operator op = new Operator();
        	        op.testResult1();
			op.testResult2();
		}

		if ( args[0].equals("dc") ){
			System.out.println("dc");
			final DecisionStructure dc = new DecisionStructure();
			//dc.testSwitch();
			//dc.doSimpleFor();
			dc.doForeach();
			//dc.doSimpleWhile();
			//dc.doSimpleDoWhile();
		}
	}



}
