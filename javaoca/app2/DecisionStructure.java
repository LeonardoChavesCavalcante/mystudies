package app2;

public class DecisionStructure{

	public void testSwitch(){

		final short x = 0;
		short v = 0;
		switch(v){

			case x:
				System.out.println("Entrou no 0");
				break;
			case 1:
				System.out.println("entrou no 1");
				break;
			case 4 :
				System.out.println("Entrou no 4");
				break;
			default:
				System.out.println("entrou default");
				break;
		}

	}

	public void doInfiniteFor(){

		System.out.println("For simples 1");
		int i=0;
		for(;;){
			System.out.println(++i);
			if (i>=10) break;

		}
	}

	public void doSimpleFor(){


		for(int i = 0; i<=5; ++i){

			System.out.println(i);

		}
	}


	public void  doForeach(){

		System.out.println("Foreach aninhado");

		int[][] dozens = {{11,12,13},{21,22,23},{31,32,33}};
		int dozen = 0;
		dozen:
		for (int[] units : dozens){
			System.out.println("DOZEN -> " +  ++dozen);
			UNIT:
			for(int num : units){
				System.out.println(num);
				if(num == 11) continue dozen;
			}
		}
	}

	public void doSimpleWhile(){

		int i = 0;
		while( ++i < 10){
			System.out.println(i);
		}
	}

	public void doSimpleDoWhile(){

		System.out.println("do while in action");
		int  i = 0;
		do{
			System.out.println(i++);
		} while(i < 10);

	}




}
